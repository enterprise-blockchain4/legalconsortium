
$(document).ready(function () {
    // Function to add a document
    $('#addDocumentForm').on('submit', function (e) {
        e.preventDefault();
        const documentData = {
            id: $('#addDocumentId').val(),
            title: $('#addDocumentTitle').val(),
            content: $('#addDocumentContent').val(),
            approvedByLawyer: $('#addDocumentLawyer').is(':checked'),
            approvedByPartner: $('#addDocumentPartner').is(':checked'),
            approvedByCourt: $('#addDocumentCourt').is(':checked')
        };

        $.ajax({
            url: '/legal/document',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(documentData),
            success: function (response) {
                alert('Document added successfully');
            },
            error: function (response) {
                alert('Failed to add document');
            }
        });
    });

    // Function to update a document
    $('#updateDocumentForm').on('submit', function (e) {
        e.preventDefault();
        const documentData = {
            id: $('#updateDocumentId').val(),
            title: $('#updateDocumentTitle').val(),
            content: $('#updateDocumentContent').val(),
            approvedByLawyer: $('#updateDocumentLawyer').is(':checked'),
            approvedByPartner: $('#updateDocumentPartner').is(':checked'),
            approvedByCourt: $('#updateDocumentCourt').is(':checked')
        };

        $.ajax({
            url: '/legal/document',
            type: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(documentData),
            success: function (response) {
                alert('Document updated successfully');
            },
            error: function (response) {
                alert('Failed to update document');
            }
        });
    });

    // Function to read a document
    $('#readDocumentForm').on('submit', function (e) {
        e.preventDefault();
        const documentId = $('#readDocumentId').val();

        $.ajax({
            url: `/legal/document/${documentId}`,
            type: 'GET',
            success: function (response) {
                const document = JSON.parse(response);
                $('#readDocumentResult').html(`
                    <p><strong>ID:</strong> ${document.id}</p>
                    <p><strong>Title:</strong> ${document.title}</p>
                    <p><strong>Content:</strong> ${document.content}</p>
                    <p><strong>Approved by Lawyer:</strong> ${document.approvedByLawyer}</p>
                    <p><strong>Approved by Partner:</strong> ${document.approvedByPartner}</p>
                    <p><strong>Approved by Court:</strong> ${document.approvedByCourt}</p>
                `);
            },
            error: function (response) {
                alert('Failed to read document');
            }
        });
    });

    // Function to approve a document
    $('#approveDocumentForm').on('submit', function (e) {
        e.preventDefault();
        const documentId = $('#approveDocumentId').val();
        const approvalType = $('#approvalType').val();
        console.log(approvalType);

        $.ajax({
            url: `/legal/document/${approvalType}/${documentId}`,
            type: 'PUT',
            success: function (response) {
                alert('Document approved successfully');
            },
            error: function (response) {
                alert('Failed to approve document');
            }
        });
    });
});
$('#getRecordCountBtn').click(function () {
    $.ajax({
        url: 'http://localhost:3000/recordCount',
        method: 'GET',
        success: function (data) {
            $('#recordDocumentCount').text(data);
        },
        error: function (xhr) {
            showAlert('Error getting record count: ' + xhr.responseText, 'danger');
        }
    });
});