const express = require('express');
const { Gateway, Wallets } = require('fabric-network');
const fs = require('fs');
const path = require('path');

const app = express();
app.use(express.json());

// Initialize documents array
let documents = [];

// Static Middleware
app.use(express.static(path.join(__dirname, 'public')));

// Endpoint to add a legal document
app.post('/legal/document', async (req, res) => {
    const { id, title, content } = req.body;
    const newDocument = { id, title, content, approvedByLawyer: false, approvedByPartner: false, approvedByCourt: false };
    res.status(204).send(newDocument);
    try {
        const result = await submitTransaction('CreateDocument', id, title, content);
        res.status(201).json({ message: 'Document added successfully', document: newDocument });
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to update a legal document
app.put('/legal/document', async (req, res) => {
    try {
        const document = req.body;
        const result = await submitTransaction('UpdateDocument', JSON.stringify(document));
        res.status(204).send('Document updated successfully');
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});

// Endpoint to read a legal document by ID
app.get('/legal/document/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const result = await evaluateTransaction('ReadDocument', id);
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(404).send(`Failed to evaluate transaction: ${error}`);
    }
});

// Endpoint to get document count
app.get('/legal/document-count', async (req, res) => {
    try {
        const result = await evaluateTransaction('GetDocumentCount');
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(500).send(`Failed to evaluate transaction: ${error}`);
    }
});

// Dynamic route for approving documents

app.put('/legal/document/:approvalType/:id', async (req, res) => {
    try {
        const { approvalType, id } = req.params;
        const validApprovalTypes = ['ApproveByLawyer', 'ApproveByPartner', 'ApproveByCourt'];

        if (!validApprovalTypes.includes(approvalType)) {
            return res.status(400).send('Invalid approval type');
        }

        const result = await submitTransaction(approvalType, id);
        res.status(204).send(result);
    } catch (error) {
        console.error(`Failed to submit transaction: ${error}`);
        res.status(500).send(`Failed to submit transaction: ${error}`);
    }
});
app.get('/recordCount', async (req, res) => {
    try {
        const result = await evaluateTransaction('GetDocumentCount');
        res.status(200).send(result);
    } catch (error) {
        console.error(`Failed to evaluate transaction: ${error}`);
        res.status(500).send(`Failed to evaluate transaction: ${error}`);
    }
});

async function getContract() {
    const walletPath = path.join(process.cwd(), 'wallet');
    const wallet = await Wallets.newFileSystemWallet(walletPath);
    const identity = await wallet.get('Admin@legalsolution.com');
    const gateway = new Gateway();
    const connectionProfile = JSON.parse(fs.readFileSync(path.resolve(__dirname, 'connection.json'), 'utf8'));

    const connectionOptions = {
        wallet, identity: identity, discovery: { enabled: false, asLocalhost: true }
    };

    await gateway.connect(connectionProfile, connectionOptions);
    const network = await gateway.getNetwork('legalsolutionchannel');
    const contract = network.getContract('legalmgt');
    return contract;
}

async function submitTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.submitTransaction(functionName, ...args);
    console.log("result", result.toString());
    return result.toString();
}

async function evaluateTransaction(functionName, ...args) {
    const contract = await getContract();
    const result = await contract.evaluateTransaction(functionName, ...args);
    return result.toString();
}

app.get('/', (req, res) => {
    res.send('Hello, Legal World!');
});

module.exports = app;
