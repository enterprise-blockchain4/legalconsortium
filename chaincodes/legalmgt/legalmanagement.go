package main

import (
    "encoding/json"
    "fmt"

    "github.com/hyperledger/fabric-contract-api-go/contractapi"
)

type LegalDocument struct {
    ID              string `json:"id"`
    Title           string `json:"title"`
    Content         string `json:"content"`
    ApprovedByLawyer bool   `json:"approvedByLawyer"`
    ApprovedByPartner bool  `json:"approvedByPartner"`
    ApprovedByCourt  bool   `json:"approvedByCourt"`
}

type LegalContract struct {
    contractapi.Contract
}

const docCountKey = "documentCount"

// CreateDocument creates a new legal document and increments the document count
func (lc *LegalContract) CreateDocument(ctx contractapi.TransactionContextInterface, id, title, content string) error {
    document := LegalDocument{
        ID:      id,
        Title:   title,
        Content: content,
    }
    documentJSON, err := json.Marshal(document)
    if err != nil {
        return err
    }
    
    // Save the new document
    err = ctx.GetStub().PutState(id, documentJSON)
    if err != nil {
        return fmt.Errorf("failed to put document to world state: %v", err)
    }
    
    // Increment the document count
    count, err := lc.GetDocumentCount(ctx)
    if err != nil {
        return err
    }
    count++
    countJSON, err := json.Marshal(count)
    if err != nil {
        return err
    }
    return ctx.GetStub().PutState(docCountKey, countJSON)
}

// ReadDocument reads a document from the ledger
func (lc *LegalContract) ReadDocument(ctx contractapi.TransactionContextInterface, id string) (*LegalDocument, error) {
    documentJSON, err := ctx.GetStub().GetState(id)
    if err != nil {
        return nil, fmt.Errorf("failed to read from world state: %v", err)
    }
    if documentJSON == nil {
        return nil, fmt.Errorf("the document %s does not exist", id)
    }

    var document LegalDocument
    err = json.Unmarshal(documentJSON, &document)
    if err != nil {
        return nil, err
    }
    return &document, nil
}

// ApproveByLawyer sets the ApprovedByLawyer flag to true for a document
func (lc *LegalContract) ApproveByLawyer(ctx contractapi.TransactionContextInterface, id string) error {
    document, err := lc.ReadDocument(ctx, id)
    if err != nil {
        return err
    }
    document.ApprovedByLawyer = true
    return lc.UpdateDocument(ctx, document)
}

// ApproveByPartner sets the ApprovedByPartner flag to true for a documentA
func (lc *LegalContract) ApproveByPartner(ctx contractapi.TransactionContextInterface, id string) error {
    document, err := lc.ReadDocument(ctx, id)
    if err != nil {
        return err
    }
    document.ApprovedByPartner = true
    return lc.UpdateDocument(ctx, document)
}

// ApproveByCourt sets the ApprovedByCourt flag to true for a document
func (lc *LegalContract) ApproveByCourt(ctx contractapi.TransactionContextInterface, id string) error {
    document, err := lc.ReadDocument(ctx, id)
    if err != nil {
        return err
    }
    document.ApprovedByCourt = true
    return lc.UpdateDocument(ctx, document)
}

// UpdateDocument updates a document in the ledger
func (lc *LegalContract) UpdateDocument(ctx contractapi.TransactionContextInterface, document *LegalDocument) error {
    documentJSON, err := json.Marshal(document)
    if err != nil {
        return err
    }
    return ctx.GetStub().PutState(document.ID, documentJSON)
}

// GetDocumentCount retrieves the total number of documents in the ledger
func (lc *LegalContract) GetDocumentCount(ctx contractapi.TransactionContextInterface) (int, error) {
    countJSON, err := ctx.GetStub().GetState(docCountKey)
    if err != nil {
        return 0, fmt.Errorf("failed to read document count from world state: %v", err)
    }
    if countJSON == nil {
        return 0, nil // No documents created yet
    }

    var count int
    err = json.Unmarshal(countJSON, &count)
    if err != nil {
        return 0, err
    }
    return count, nil
}

func main() {
    chaincode, err := contractapi.NewChaincode(new(LegalContract))
    if err != nil {
        fmt.Printf("Error creating legal document chaincode: %s", err.Error())
        return
    }
    if err := chaincode.Start(); err != nil {
        fmt.Printf("Error starting legal document chaincode: %s", err.Error())
    }
}
